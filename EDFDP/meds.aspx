﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="meds.aspx.vb" Inherits="meds" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="height: 594px">
    <form id="form1" runat="server">
        <div style="height: 729px">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataSourceID="SqlDataSource1" Height="539px" style="font-size: large" Width="1140px">
                <Columns>
                    <asp:BoundField DataField="PID" HeaderText="PID" InsertVisible="False" SortExpression="PID" />
                    <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                    <asp:BoundField DataField="Medication_Code" HeaderText="Medication_Code" SortExpression="Medication_Code" />
                    <asp:BoundField DataField="Medication_Name" HeaderText="Medication_Name" SortExpression="Medication_Name" />
                    <asp:BoundField DataField="Dosage" HeaderText="Dosage" SortExpression="Dosage" />
                    <asp:BoundField DataField="Last modified" HeaderText="Last modified" SortExpression="Last modified" />
                    <asp:BoundField DataField="Modified by" HeaderText="Modified by" SortExpression="Modified by" />
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Patient_Medgiven]"></asp:SqlDataSource>
        </div>
    </form>
</body>
</html>

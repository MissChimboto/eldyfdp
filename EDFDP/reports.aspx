﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="reports.aspx.vb" Inherits="reports" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="height: 541px">
    <form id="form1" runat="server">
        <div style="height: 543px">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="Identifier" DataSourceID="SqlDataSource1" Height="441px" style="font-size: x-large" Width="716px">
                <Columns>
                    <asp:BoundField DataField="Identifier" HeaderText="Identifier" InsertVisible="False" ReadOnly="True" SortExpression="Identifier" />
                    <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description" />
                    <asp:BoundField DataField="Illness_Name" HeaderText="Illness_Name" SortExpression="Illness_Name" />
                    <asp:BoundField DataField="Illness_Year" HeaderText="Illness_Year" SortExpression="Illness_Year" />
                </Columns>
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FFF1D4" />
                <SortedAscendingHeaderStyle BackColor="#B95C30" />
                <SortedDescendingCellStyle BackColor="#F1E5CE" />
                <SortedDescendingHeaderStyle BackColor="#93451F" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Reports]"></asp:SqlDataSource>
        </div>
    </form>
</body>
</html>

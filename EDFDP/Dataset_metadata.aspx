﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Dataset_metadata.aspx.vb" Inherits="Dataset_metadata" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            font-size: x-large;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 635px; text-align: justify">
            <asp:FormView ID="FormView1" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataSourceID="SqlDataSource1" GridLines="Both" Height="435px" style="font-size: x-large; text-align: left" Width="909px">
                <EditItemTemplate>
                    Title:
                    <asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' />
                    <br />
                    MetadataID:
                    <asp:TextBox ID="MetadataIDTextBox" runat="server" Text='<%# Bind("MetadataID") %>' />
                    <br />
                    Description:
                    <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />
                    <br />
                    Last Modified:
                    <asp:TextBox ID="Last_ModifiedTextBox" runat="server" Text='<%# Bind("[Last Modified]") %>' />
                    <br />
                    License:
                    <asp:TextBox ID="LicenseTextBox" runat="server" Text='<%# Bind("License") %>' />
                    <br />
                    Language:
                    <asp:TextBox ID="LanguageTextBox" runat="server" Text='<%# Bind("Language") %>' />
                    <br />
                    Access Rights:
                    <asp:TextBox ID="Access_RightsTextBox" runat="server" Text='<%# Bind("[Access Rights]") %>' />
                    <br />
                    Country:
                    <asp:TextBox ID="CountryTextBox" runat="server" Text='<%# Bind("Country") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </EditItemTemplate>
                <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <InsertItemTemplate>
                    Title:
                    <asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' />
                    <br />
                    MetadataID:
                    <asp:TextBox ID="MetadataIDTextBox" runat="server" Text='<%# Bind("MetadataID") %>' />
                    <br />
                    Description:
                    <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />
                    <br />
                    Last Modified:
                    <asp:TextBox ID="Last_ModifiedTextBox" runat="server" Text='<%# Bind("[Last Modified]") %>' />
                    <br />
                    License:
                    <asp:TextBox ID="LicenseTextBox" runat="server" Text='<%# Bind("License") %>' />
                    <br />
                    Language:
                    <asp:TextBox ID="LanguageTextBox" runat="server" Text='<%# Bind("Language") %>' />
                    <br />
                    Access Rights:
                    <asp:TextBox ID="Access_RightsTextBox" runat="server" Text='<%# Bind("[Access Rights]") %>' />
                    <br />
                    Country:
                    <asp:TextBox ID="CountryTextBox" runat="server" Text='<%# Bind("Country") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </InsertItemTemplate>
                <ItemTemplate>
                    Title:
                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Bind("Title") %>' />
                    <br />
                    MetadataID:
                    <asp:Label ID="MetadataIDLabel" runat="server" Text='<%# Bind("MetadataID") %>' />
                    <br />
                    Description:
                    <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Bind("Description") %>' />
                    <br />
                    Last Modified:
                    <asp:Label ID="Last_ModifiedLabel" runat="server" Text='<%# Bind("[Last Modified]") %>' />
                    <br />
                    License:
                    <asp:Label ID="LicenseLabel" runat="server" Text='<%# Bind("License") %>' />
                    <br />
                    Language:
                    <asp:Label ID="LanguageLabel" runat="server" Text='<%# Bind("Language") %>' />
                    <br />
                    Access Rights:
                    <asp:Label ID="Access_RightsLabel" runat="server" Text='<%# Bind("[Access Rights]") %>' />
                    <br />
                    Country:
                    <asp:Label ID="CountryLabel" runat="server" Text='<%# Bind("Country") %>' />
                    <br />

                </ItemTemplate>
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            </asp:FormView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Datasets_metadata]"></asp:SqlDataSource>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="auto-style1">&nbsp;</span><asp:HyperLink ID="HyperLink1" runat="server" CssClass="auto-style1" NavigateUrl="~/Datasets.aspx" style="text-align: center">Click to access patient details</asp:HyperLink>
            <br />
        </div>
    </form>
</body>
</html>

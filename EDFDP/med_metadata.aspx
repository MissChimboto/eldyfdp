﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="med_metadata.aspx.vb" Inherits="med_metadata" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="height: 647px">
    <form id="form1" runat="server">
        <div style="height: 685px">
            <asp:FormView ID="FormView1" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataSourceID="SqlDataSource1" GridLines="Both" Height="520px" style="font-size: xx-large" Width="875px">
                <EditItemTemplate>
                    Title:
                    <asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' />
                    <br />
                    MetadataID:
                    <asp:TextBox ID="MetadataIDTextBox" runat="server" Text='<%# Bind("MetadataID") %>' />
                    <br />
                    Description:
                    <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />
                    <br />
                    Last modified:
                    <asp:TextBox ID="Last_modifiedTextBox" runat="server" Text='<%# Bind("[Last modified]") %>' />
                    <br />
                    License:
                    <asp:TextBox ID="LicenseTextBox" runat="server" Text='<%# Bind("License") %>' />
                    <br />
                    Access rights:
                    <asp:TextBox ID="Access_rightsTextBox" runat="server" Text='<%# Bind("[Access rights]") %>' />
                    <br />
                    Language:
                    <asp:TextBox ID="LanguageTextBox" runat="server" Text='<%# Bind("Language") %>' />
                    <br />
                    Country:
                    <asp:TextBox ID="CountryTextBox" runat="server" Text='<%# Bind("Country") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </EditItemTemplate>
                <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <InsertItemTemplate>
                    Title:
                    <asp:TextBox ID="TitleTextBox" runat="server" Text='<%# Bind("Title") %>' />
                    <br />
                    MetadataID:
                    <asp:TextBox ID="MetadataIDTextBox" runat="server" Text='<%# Bind("MetadataID") %>' />
                    <br />
                    Description:
                    <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />
                    <br />
                    Last modified:
                    <asp:TextBox ID="Last_modifiedTextBox" runat="server" Text='<%# Bind("[Last modified]") %>' />
                    <br />
                    License:
                    <asp:TextBox ID="LicenseTextBox" runat="server" Text='<%# Bind("License") %>' />
                    <br />
                    Access rights:
                    <asp:TextBox ID="Access_rightsTextBox" runat="server" Text='<%# Bind("[Access rights]") %>' />
                    <br />
                    Language:
                    <asp:TextBox ID="LanguageTextBox" runat="server" Text='<%# Bind("Language") %>' />
                    <br />
                    Country:
                    <asp:TextBox ID="CountryTextBox" runat="server" Text='<%# Bind("Country") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </InsertItemTemplate>
                <ItemTemplate>
                    Title:
                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Bind("Title") %>' />
                    <br />
                    <br />
                    MetadataID:
                    <asp:Label ID="MetadataIDLabel" runat="server" Text='<%# Bind("MetadataID") %>' />
                    <br />
                    <br />
                    Description:
                    <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Bind("Description") %>' />
                    <br />
                    <br />
                    Last modified:
                    <asp:Label ID="Last_modifiedLabel" runat="server" Text='<%# Bind("[Last modified]") %>' />
                    <br />
                    <br />
                    License:
                    <asp:Label ID="LicenseLabel" runat="server" Text='<%# Bind("License") %>' />
                    <br />
                    <br />
                    Access rights:
                    <asp:Label ID="Access_rightsLabel" runat="server" Text='<%# Bind("[Access rights]") %>' />
                    <br />
                    <br />
                    Language:
                    <asp:Label ID="LanguageLabel" runat="server" Text='<%# Bind("Language") %>' />
                    <br />
                    <br />
                    Country:
                    <asp:Label ID="CountryLabel" runat="server" Text='<%# Bind("Country") %>' />
                    <br />

                </ItemTemplate>
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            </asp:FormView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [medgiven_metadata]"></asp:SqlDataSource>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="HyperLink1" runat="server" style="font-size: x-large; font-weight: 700" NavigateUrl="~/meds.aspx">Click to access patient medication</asp:HyperLink>
        </div>
    </form>
</body>
</html>

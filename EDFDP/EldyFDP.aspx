﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EldyFDP.aspx.vb" Inherits="EldyFDP" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="height: 588px">
            <asp:FormView ID="FormView1" runat="server" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="Identifier" DataSourceID="SqlDataSource1" GridLines="Both" Height="467px" style="text-align: center; font-size: xx-large" Width="745px">
                <EditItemTemplate>
                    Identifier:
                    <asp:Label ID="IdentifierLabel1" runat="server" Text='<%# Eval("Identifier") %>' />
                    <br />
                    FDP_Name:
                    <asp:TextBox ID="FDP_NameTextBox" runat="server" Text='<%# Bind("FDP_Name") %>' />
                    <br />
                    Description:
                    <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />
                    <br />
                    Institution:
                    <asp:TextBox ID="InstitutionTextBox" runat="server" Text='<%# Bind("Institution") %>' />
                    <br />
                    Contact:
                    <asp:TextBox ID="ContactTextBox" runat="server" Text='<%# Bind("Contact") %>' />
                    <br />
                    <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
                    &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </EditItemTemplate>
                <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                <InsertItemTemplate>
                    Identifier:
                    <asp:TextBox ID="IdentifierTextBox" runat="server" Text='<%# Bind("Identifier") %>' />
                    <br />
                    FDP_Name:
                    <asp:TextBox ID="FDP_NameTextBox" runat="server" Text='<%# Bind("FDP_Name") %>' />
                    <br />
                    Description:
                    <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />
                    <br />
                    Institution:
                    <asp:TextBox ID="InstitutionTextBox" runat="server" Text='<%# Bind("Institution") %>' />
                    <br />
                    Contact:
                    <asp:TextBox ID="ContactTextBox" runat="server" Text='<%# Bind("Contact") %>' />
                    <br />
                    <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
                    &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
                </InsertItemTemplate>
                <ItemTemplate>
                    Identifier:
                    <asp:Label ID="IdentifierLabel" runat="server" Text='<%# Eval("Identifier") %>' />
                    <br />
                    <br />
                    FDP_Name:
                    <asp:Label ID="FDP_NameLabel" runat="server" Text='<%# Bind("FDP_Name") %>' />
                    <br />
                    <br />
                    Description:
                    <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Bind("Description") %>' />
                    <br />
                    <br />
                    Institution:
                    <asp:Label ID="InstitutionLabel" runat="server" Text='<%# Bind("Institution") %>' />
                    <br />
                    <br />
                    Contact:
                    <asp:Label ID="ContactLabel" runat="server" Text='<%# Bind("Contact") %>' />
                    <br />

                </ItemTemplate>
                <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
            </asp:FormView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="<%$ ConnectionStrings:ConnectionString.ProviderName %>" SelectCommand="SELECT [Identifier], [FDP_Name], [Description], [Institution], [Contact] FROM [FDP]"></asp:SqlDataSource>
            <br />
            <asp:Button ID="Button1" runat="server" Height="37px" PostBackUrl="~/Dataset_metadata.aspx" style="font-size: large; font-weight: 700" Text="Patients" Width="123px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
            <asp:Button ID="Button2" runat="server" Height="37px" PostBackUrl="~/stock_metadat.aspx" style="font-size: large; font-weight: 700" Text="Stock" Width="125px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button3" runat="server" Height="36px" style="font-size: large; font-weight: 700" Text="Reports" Width="112px" PostBackUrl="~/report_metadata.aspx" />
        </div>
    </form>
</body>
</html>
